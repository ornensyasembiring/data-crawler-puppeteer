const puppeteer = require("puppeteer");

const EMAIL = 'kuroyukifm9090@gmail.com';
const PASSWORD = 'TerbanglahKau123';

const LINKEDIN_URL = "https://www.linkedin.com/";

if (process.argv[2] !== undefined) {
  (async () => {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();

    try {
      // Navigate to LinkedIn login page.
      await page.setViewport({ width: 1200, height: 768 });
      await page.goto(LINKEDIN_URL, { waitUntil: 'domcontentloaded' });

      // Locate and type into the email input field using XPath.
      const emailInput = await page.$x('//input[@name="session_key"]');
      if (emailInput.length > 0) {
        await emailInput[0].type(EMAIL);
      }

      // Locate and type into the password input field using XPath.
      const passwordInput = await page.$x('//input[@name="session_password"]');
      if (passwordInput.length > 0) {
        await passwordInput[0].type(PASSWORD);
      }

      // Click the "Sign in" button using XPath.
      // const signInButton = await page.$x('//*[@id="main-content"]/section[1]/div/div/form/div[2]/button');

      //using CSS selector (kinda work but idk)
      const signInButton = await page.$('.sign-in-form__submit-btn--full-width')
      if (signInButton) {
        await signInButton.click();
        await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
        console.log('Logged in to LinkedIn!');
      } else {
        console.log('Sign-in button not found.');
      }

    } catch (e) {
      console.log(e);
    }

    // finally {
    //   await browser.close();
    // }
  })();
}
